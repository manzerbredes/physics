
draw_arrow=function(p,x1,y1,x2,y2,elt=null,canvas,skiparrow=false,flip=false){
    var offset=5
    
    // Reduce the length of the vector to have a better tip location
    let v=p.createVector(x2-x1,y2-y1)
    v.setMag(v.mag()-offset*2)
    x2=x1+v.x
    y2=y1+v.y

    // Draw the vector
    if(!skiparrow){
        p.push()
        p.strokeWeight(5)
        p.line(x1,y1,x2,y2)
        var angle = p.atan2(y1 - y2, x1 - x2); //gets the angle of the line
        p.translate(x2, y2); //translates to the destination vertex
        p.rotate(angle-p.HALF_PI); //rotates the arrow point
        p.triangle(-offset*0.8, offset, offset*0.8, offset, 0, -offset/2); //draws the arrow point as a triangle
        p.pop();
    }
    
    if(elt != null){
        // Compute center of the vector
        v.setMag(v.mag()/2)
        center=p.createVector(x1,y1).add(v)

        // Compute x and y factor to offset elt
        yfactor=p.abs(p.PI/2-p.abs(angle))/(p.PI/2)
        xfactor=1-yfactor
        justify=15

        if(flip){
            xfactor=-xfactor
            yfactor=-yfactor
        }
            
        
        if(angle>0){
            yfactor=-yfactor
        }

        cp=canvas.position()
        elt_x=cp.x+center.x+justify*xfactor-5
        elt_y=cp.y+center.y-justify*yfactor-elt.elt.offsetHeight/2
        if (elt_x<cp.x ||  elt_y < cp.y || elt_x>(canvas.width+cp.x) || elt_y>(canvas.height+cp.y)){
            elt.elt.style.display="none"
        } else {
            elt.elt.style.display="block"
        }

        // Move element
        elt.position(elt_x,elt_y)
    }    
}
