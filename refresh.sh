#!/usr/bin/env bash

wai=$(dirname $(readlink -f "$0")) # Current script directory
public=${wai}/public
projects=${wai}/projects
template=${wai}/template.html
main="projectile"

# Check if auto enabled
[ $# -gt 0 ] && { echo -e "$(find ${projects}/ -type f)\n$template" | entr $0; exit 0; }

# Clean before
rm -rf $public/projects
rm -rf $public/*.html
mkdir -p $public/projects

# Build links
build_links() {
    links=""
    for p in $(find ${projects}/ -maxdepth 1 -mindepth 1 -type d)
    do
        name=$(basename $p)
        beautiful_name=$(cat $p/name.txt)
        [ "$name" == "$main" ]  && link="./index.html"  || link=${name}.html
        [ $name == $1 ] && active="active" || active=""
        
        links="${links}\n"'<a href="'$link'" class="btn btn-primary '$active'">'${beautiful_name}'</a>'
    done
    tmp=$(mktemp)
    echo -e "$links" > $tmp
    echo $tmp
}

for p in $(find ${projects}/ -maxdepth 1 -mindepth 1 -type d)
do
    name=$(basename $p)
    [ "$name" == "$main" ]  && html=${public}/index.html  || html=${public}/${name}.html
    js=./projects/$name/index.js
    content=$p/index.html
    beautiful_name=$(cat $p/name.txt)

    # Create HTML page
    cp -r $p $public/projects/
    cat $template |sed "/\${CONTENT}/r $content"|sed '/\${CONTENT}/d' > $html
    sed -i "s#\${JS}#${js}#g" $html
    sed -i "s#\${project_name}#${name}#g" $html
    sed -i "s#\${beautiful_name}#${beautiful_name}#g" $html
    echo $html

    
    # Create links
    links_file=$(build_links $name)
    sed -i "/\${LINKS}/r $links_file" $html
    sed -i '/\${LINKS}/d' $html
done
